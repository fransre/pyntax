# Pyntax

Pyntax is a web service to learn to quickly fix simple syntax errors in your Python programs

Pyntax is written in Javascript and uses Pyodide to run Python in the browser.

```mermaid
flowchart TD
    Start --> A[Pick a random template]
    A --> B[Substitute placeholders]
    B --> |Valid program| C[Introduce a random bug]
    C --> |Invalid program| D[Ask the user to fix the bug]
    D --> |Corrected program| E{Corrected program\nis equal to\nvalid program?}
    D --> |After 10 seconds| F[Show the syntax error]
    E --> |False| D
    E --> |True| A
    F --> D
```
  