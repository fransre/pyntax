class Program {
  correct_program = undefined
  wrong_program = undefined
  checks = 0

  constructor() {
    this.correct_program = this.random_correct_program()
    this.wrong_program = this.random_wrong_program(this.correct_program)
  }

  async check(code) {
    this.checks++
    return await Python.syntax_error(code)
  }

  is_checked_once() {
    return this.checks == 1
  }

  static templates = [
    "<number1> = input(\"<prompt>\")\n" +
    "<number2> = input(\"<prompt>\")\n" +
    "if <number1> == <number2>:\n" +
    "    print(\"equal\")\n",
    // ---------------------------------
    "def <function1>(<number2>):\n" +
    "    if <number2> >= 9:\n" +
    "        print(\"large\")\n" +
    "    else:\n" +
    "        print(\"small\")\n" +
    "<function1>(<int>)\n",
    // ---------------------------------
    "time = \"13:<int>\"\n" +
    "def <function1>():\n" +
    "    if time > \"12\":\n" +
    "        print(\"afternoon\")\n" +
    "    else:\n" +
    "        print(\"morning\")\n" +
    "<function1>()\n",
    // ---------------------------------
    "<list1> = [<int>, <int>, <int>]\n" +
    "for <number1> in <list1>:\n" +
    "    print(<number1>)\n" +
    "print(\"done\")\n",
    // ---------------------------------
    "<list1> = [<int>, <int>, <int>]\n" +
    "<number1> = int(input(\"<prompt>\"))\n" +
    "print(<list1>[<number1>])\n",
    // ---------------------------------
    "<dict1> = {\"a\":<int>, \"b\":<int>, \"c\":<int>}\n" +
    "<number1> = int(input(\"<prompt>\"))\n" +
    "if <dict1>[\"a\"] == <number1>:\n" +
    "    print(\"a\")\n" +
    "elif <dict1>[\"b\"] == <number1>:\n" +
    "    print(\"b\")\n" +
    "else:\n" +
    "    print(\"c\")\n",
    // ---------------------------------
    "<number1> = <float>\n" +
    "<number2> = <float>\n" +
    "<number3> = <number1> + <number2>\n" +
    "print(\"The sum of\", <number1>, \"and\", <number2>, \"is\", <number3>)\n",
    // ---------------------------------
    "<number1> = int(input(\"<prompt>\"))\n" +
    "<number2> = 0\n" +
    "while <number1> > 0:\n" +
    "    <number2> += <number1>\n" +
    "    <number1> -= 1\n" +
    "print(\"The sum is\", <number2>)\n",
    // ---------------------------------
    "def <function1>(<number1>, <number2>):\n" +
    "    while <number2>:\n" +
    "       <number1>, <number2> = <number2>, <number1> % <number2>\n" +
    "    return <number1>\n" +
    "<number3> = <function1>(<int>, <int>)\n",
    // ---------------------------------
    "def <function1>(<number1>):\n" +
    "    if <number1> <= 1:\n" +
    "        return <number1>\n" +
    "    else:\n" +
    "        return <function1>(<number1> - 1) + <function1>(<number1> - 2)\n" +
    "<function1>(int(input(\"<prompt>\")))\n",
    // ---------------------------------
    "def <function1>(<number1>):\n" +
    "    if <number1> > 1:\n" +
    "        <function1>(<number1> // 2)\n" +
    "        print(<number1> % 2, end = \"\")\n" +
    "<function1>(<int>)\n",
    // ---------------------------------
    "<number1> = \"<string>\"\n" +
    "<number2> = \"<string>\"\n" +
    "sorted_<number1> = sorted(<number1>)\n" +
    "sorted_<number2> = sorted(<number2>)\n" +
    "if sorted_<number1> == sorted_<number2>:\n" +
    "    print(\"anagram\")\n",
    // ---------------------------------
    "<number1> = int(input(\"<prompt>\"))\n" +
    "for <number2> in range(1, 11):\n" +
    "    print(<number2>, \"*\", <number1>, \"=\", <number1> * <number2>)\n" +
    "print(\"=\" * 12)\n",
    // ---------------------------------
    "<number1> = int(input(\"<prompt>\"))\n" +
    "for <number2> in range(<number1>, 0, -1):\n" +
    "    for <number3> in range(<number2>, 0, -1):\n" +
    "        if <number2> == 1 or <number2> == <number1> or <number3> == 1 or <number3> == <number2>:\n" +
    "            print(\"*\", end=\"\")\n" +
    "        else:\n" +
    "            print(\" \", end=\"\")\n",
    // ---------------------------------
    "while True:\n" +
    "    <number1> = int(input(\"<prompt>\"))\n" +
    "    if <number1> == 1:\n" +
    "        break\n" +
    "    print(<number1> - 1)\n",
    // ---------------------------------
    "<number1> = int(input(\"<prompt>\"))\n" +
    "if <number1> == 1 or <number1> == 2:\n" +
    "    print(\"one or two\")\n" +
    "elif <number1> < 10:\n" +
    "    print(\"less than ten\")\n" +
    "else:\n" +
    "    print(\"large\")\n",
  ]

  static substitutions = new Map([
    ["dict", new Set(["s", "dictionary", "mapping", "translations"])],
    ["float", new Set(["1.5", "6.3", "2.83", "4.97", "12.31", "372.09", "9237.830"])],
    ["function", new Set(["some_function", "compute", "show_information", "check", "do_something", "flock"])],
    ["int", new Set(["1", "2", "5", "7", "16", "21", "93", "142", "823", "3491"])],
    ["list", new Set(["numbers", "totals", "averages", "heights"])],
    ["number", new Set(["a", "b", "x", "y", "number", "first", "last", "level", "average", "total", "user_input", "option"])],
    ["prompt", new Set(["Give a number:", "Number:", "How many?", "Input:"])],
    ["string", new Set(["random text", "a sentence", "test test", "my name"])],
  ])

  static bugs = [
    [ /:\n/, "\n" ], // forget the colon
    [ /:\n/, ";\n" ], // put a semicolon instead of a colon
    [ /(:\n) {4}/, "$1" ], // forget to indent after a colon
    [ /==/, "=" ], // replace a comparison by an assignment
    [ /(\w+)\(([^\)]+)\)/, "$1 $2" ], // forget the parentheses around a function call
    [ /\(\)/, "" ], // forget parentheses after a function definition or call without arguments
    [ /(\w+)\[([^\]]+)\]/, "$1($2)" ], // replace square brackets with parentheses
    [ /(\w+)\[([^\]]+)\]/, "$1{$2}" ], // replace square brackets with curly brackets
    [ /\{([^\}]+)\}/, "($1)" ], // replace curly brackets with parentheses
    [ /\{([^\}]+)\}/, "[$1]" ], // replace curly brackets with square brackets
    [ /[\)\]\}]/, "" ], // remove a closing parenthesis or bracket
    [ /,/, "" ], // remove a comma
    [ /,/, "." ], // replace a comma with a dot
    [ /def/, "dev" ], // misspell the keyword def
    [ /elif/, "elsif" ], // misspell the keyword elif
    [ /else/, "esle" ], // misspell the keyword else
    [ /while/, "whil" ], // misspell the keyword while
    [ /(\d+)\.(\d+)/, "$1,$2" ], // use the wrong decimal separator
  ]

  random_correct_program() {
    let code = Program.templates.random()
    const clone_of_substitutions = structuredClone(Program.substitutions)
    while(true) {
      const tag = code.match(/<([a-z]+)(\d*)>/)
      if (!tag)
        return code
      const pattern = new RegExp(tag[0], tag[2] ? "g" : "")
      const choices = clone_of_substitutions.get(tag[1])
      const substitution = Array.from(choices).random()
      code = code.replace(pattern, substitution)
      choices.delete(substitution)
    }
  }

  random_wrong_program(code) {
    while(true) {
      const bug = Program.bugs.random()
      const occurences = Array.from(code.matchAll(new RegExp(bug[0], "gd")))
      if (occurences.length > 0) {
        const occurence = occurences.random()
        const from = occurence.indices[0][0]
        const to = occurence.indices[0][1]
        return code.slice(0, from) + code.slice(from, to).replace(bug[0], bug[1]) + code.slice(to)
      }
    }
  }

  is_correct(code) {
    return code.trim() === this.correct_program.trim()
  }
}

class Python {
  static promise = Python.load()
  
  static async load() {
    return await loadPyodide({ stdin: () => "1", stdout: () => {} })
  }

  static async syntax_error(code) {
    const pyodide = await Python.promise
    try {
      pyodide.runPython(code)
    } catch (exception) {
      return exception.toString().replace(/\n  File "\/.*\n.*/g, "")
    }
    return null
  }
}

class Timer {
  element = undefined
  start_time = undefined
  interval_id = undefined
  successes = 0
  static timeout_in_seconds = 300

  constructor(element) {
    this.element = element
    this.start_time = new Date()
    setTimeout(() => { this.stop() }, Timer.timeout_in_seconds * 1000)
    this.interval_id = setInterval(() => { this.info() }, 1000)
    this.info()
  }

  info() {
    const seconds = Math.min(Math.round(((new Date()) - this.start_time) / 1000), Timer.timeout_in_seconds)
    this.element.textContent = `${this.successes} successes in ${seconds} seconds`
  }

  success() {
    this.successes++
    this.info()
  }

  stop() {
    clearInterval(this.interval_id)
    this.info()
    alert(`Well done! ${this.element.textContent}`)
    Timer.successes_without_timer = 0
  }

  static successes_without_timer = 0

  static enough_successes_without_timer() {
    return this.successes_without_timer > 1
  }

  static success_without_timer() {
    this.successes_without_timer++
  }
}

const program_element = document.getElementById("program")
const error_element = document.getElementById("error")
const check_button = document.getElementById("check")
const reset_button = document.getElementById("reset")
const start_button = document.getElementById("start")
const hint_element = document.getElementById("hint")
const info_element = document.getElementById("info")
const message_element = document.getElementById("message")
let program
let show_error_timeout_id
let timer

check_button.addEventListener("click", check)
reset_button.addEventListener("click", reset)
start_button.addEventListener("click", start)

Array.prototype.random = function() {
  return this[Math.floor(Math.random() * this.length)]
}

Element.prototype.hide = function() {
  this.style.display = "none"
}

Element.prototype.show = function() {
  this.style.display = "block"
}

program_element.addEventListener("keydown",
  function(event) { 
    if ((event.ctrlKey || event.metaKey) && (event.keyCode == 13 || event.keyCode == 10)) {
      document.querySelectorAll("button").forEach(button => {
        if (button.style.display == "block")
          button.click()
      })
      hint_element.classList.add("hidden") // hide it forever 
    }
    else if (event.keyCode == 9) {
      event.preventDefault()
      const text = program_element.value
      const position = program_element.selectionStart
      const tab_length = 4
      const indentation = " ".repeat(tab_length)
      if (event.shiftKey) {
        if (text.slice(position - tab_length, position) == indentation) {
          program_element.value = text.slice(0, position - tab_length) + text.slice(position)
          program_element.selectionStart = program_element.selectionEnd = position - tab_length
        }
      }
      else {
        program_element.value = text.slice(0, position) + indentation + text.slice(position)
        program_element.selectionStart = program_element.selectionEnd = position + tab_length
      }
    }
  }
)

next()

function next() {
  program = new Program()
  reset()
}

function reset() {
  program_element.value = program.wrong_program
  program_element.selectionStart = program_element.selectionEnd = 0
  check()
}

function message(text, success) {
  message_element.classList.remove("animate", "success", "failure")
  message_element.textContent = text
  void message_element.offsetWidth; // make the DOM rerender the message element
  message_element.classList.add("animate", success ? "success" : "failure")
}

async function check() {
  clearTimeout(show_error_timeout_id)
  error_element.hide()
  check_button.hide()
  reset_button.hide()
  hint_element.hide()
  start_button.hide()
  const error = await program.check(program_element.value)
  if (error) {
    error_element.textContent = error
    show_error_timeout_id = setTimeout(() => { error_element.show() }, 10000)
    check_button.show()
    hint_element.show()
    program_element.focus()
    if (!program.is_checked_once()) {
      console.log("error")
      message("No, there is still a syntax error", false)
    }
    return
  }
  if (!error && program.is_checked_once())
    next()
  if (!program.is_correct(program_element.value)) {
    reset_button.show()
    hint_element.show()
    return
  }
  message("Yes, you fixed the syntax error!", true)
  if (!timer && Timer.enough_successes_without_timer())
    start_button.show()
  else
    next()
  hint_element.show()
  if (timer)
    timer.success()
  else
    Timer.success_without_timer()
}

function show_error() {
  error_element.show()
}

function start() {
  timer = new Timer(info_element)
  start_button.hide()
  next()
}
